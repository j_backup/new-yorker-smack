# New Yorker - SMACK

## Quickstart

1. From the root of the project build the JAR for the import tool with: `sbt assembly`.
2. Put the tar with the data in the root folder of the project.
3. Start all containers with `docker-compose up -d --build`.
4. Follow the logs of the import tool with first getting the name `docker ps` and then running `docker logs <container_name> -f`. 
It might be, that the import tool came up too fast. In that case just restart the container with `docker container restart <container_name>`.
5. The logs will show "<table_name> DONE!!!" and a print of the first 25 lines after the tool is done importing a file from the data set.

## How to build project

This projects needs to have sbt and Java 8 installed.

To build the data import tool JAR call `sbt assembly` in the root folder.

The FAT JAR should appear in `taget/scala-2.13/New-Yorker-SMACK-assembly-1.0.1.jar`. Just in case in has been packaged in Git as well.

The program will connect to Cassandra and read all the JSON files from the Yahoo data set into separate Cassandra tables using Akka Streams.
Furthermore it creates all the needed keyspaces and tables on its own.
After it imported a file completely, it will run a SELECT query to check, if the import worked.
If something can't be parsed for the import, the logger will show an error.
The program expects the files to be already unpacked from the .tar.

## How to use docker

To build the docker file use `docker build . --build-arg PATH_TO_DATA_SET=./ DATA_SET_NAME=yelp_dataset.tar --tag smack-reader`.
The path to the data needs to be added at build time, because the container unpacks it directly. A nicer solution would be to unpack the data before hand and using a volume. To avoid complexity, the first idea was chosen.
Unfortunately this makes the container pretty big and it takes some time to build the first stage.  

To run the container use `docker run --network smack -e CASSANDRA_ADDRESS="smack-cassandra" -e CASSANDRA_PORT="9042" smack-reader`.

If it is also desired to run Cassandra in Docker, use this command to start it up `docker run -p 9042:9042 --name smack-cassandra --network smack-net cassandra -d`.

Alternatively run `docker-compose up -d --build` to start and build Cassandra and the import tool together. It might be, that the import tool came up too fast. In that case just restart the container with `docker container restart <container_name>`. 
The container will through a java exception, that it could not connect to Cassandra. This means Cassandra did not start completely yet. 

For a CQLSH use `docker run -it --network smack --rm cassandra cqlsh smack-cassandra`.

### Dockerfile parameters

See the rest of the README for examples.

During build time set these:

ARGS:

- PATH_TO_DATA_SET=./
- DATA_SET_NAME=yelp_dataset.tar

During start up set these:

ENV:

- CASSANDRA_ADDRESS '127.0.0.1'
- CASSANDRA_PORT '9042'

## Monitoring the results

Check the logs of the import tool with `docker logs <container_name> -f` to follow, when the container is done importing the data or if there are any errors. 

## Useful commands (Unix)

```bash
# Unpack tar in current directory
tar -xvzf yelp_dataset.tar

# Unpack tar in specific directory
# Ensure that dir exist first
ls ./path/to/dir
mkdir ./path/to/dir
tar -xf yelp_dataset.tar -C ./path/to/dir

# Follow docker log
docker logs <container_name> -f
```
