package de.joge.smack

case class Checkin (
                     business_id: String,
                     date: String
                   )
