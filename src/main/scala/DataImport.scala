package de.joge.smack

import java.nio.file.Paths

import akka.actor.ActorSystem
import akka.stream._
import akka.stream.alpakka.cassandra.scaladsl.CassandraFlow
import akka.stream.scaladsl._
import akka.util.ByteString
import com.datastax.driver.core.{Cluster, PreparedStatement, ResultSet, Session}
import io.circe.parser._
import io.circe.generic.auto._
import io.circe.generic.extras.Configuration

import scala.collection.mutable
import scala.concurrent.Future

object DataImport {
  // Set table names in the object scope for easy access
  val keyspaceName = "smack"
  val bestBusinessTableName = "best_business"
  val photoTableName = "photos"
  val tipTableName = "tips"
  val reviewTableName = "reviews"
  val userTableName = "users"
  val checkinTableName = "checkins"
  val businessTableName = "businesses"
  val mapOfTables: mutable.Map[String, Boolean] = mutable.Map(bestBusinessTableName -> false, photoTableName -> false, checkinTableName -> false,
    userTableName -> false, tipTableName -> false, reviewTableName -> false, businessTableName -> false)

  def main(args: Array[String]): Unit = {
    // Set all implicits in the context
    implicit val system: ActorSystem = ActorSystem("SMACK")
    implicit val mat: ActorMaterializer = ActorMaterializer()
    implicit val config: Configuration = Configuration.default.withDefaults
    implicit val ec: scala.concurrent.ExecutionContext = scala.concurrent.ExecutionContext.global

    var cassandraAddress = "127.0.0.1"
    var cassandraPort = 9042

    // Parse args
    args.sliding(2, 2).toList.collect {
      case Array("--cassandraAddress", cA: String) => cassandraAddress = cA
      case Array("--cassandraPort", cP: String) => cassandraPort = cP.toInt
    }

    // Build connection to Cassandra
    implicit val cSession: Session = Cluster.builder
      .addContactPoint(cassandraAddress)
      .withPort(cassandraPort)
      .build
      .connect()

    setupCassandraDB(cSession)

    // Set sources for all files
    val linesBusiness: Source[Business, Future[IOResult]] = importDataFromFile("business.json")
      .map(decode[Business](_).getOrElse(null))
    val linesPhoto: Source[Photo, Future[IOResult]] = importDataFromFile("photo.json")
      .map(decode[Photo](_).getOrElse(null))
    val linesCheckin: Source[Checkin, Future[IOResult]] = importDataFromFile("checkin.json")
      .map(decode[Checkin](_).getOrElse(null))
    val linesTip: Source[Tip, Future[IOResult]] = importDataFromFile("tip.json")
      .map(decode[Tip](_).getOrElse(null))
    val linesUser: Source[User, Future[IOResult]] = importDataFromFile("user.json")
      .map(decode[User](_).getOrElse(null))
    val linesReview: Source[Review, Future[IOResult]] = importDataFromFile("review.json")
      .map(decode[Review](_).getOrElse(null))


    // Preparing all import statements for Cassandra
    // The statements will be transferred over to Cassandra once at the beginning, so that only the data needs to be send after that
    val bestBusinessStatement = cSession.prepare(s"INSERT INTO $keyspaceName.$bestBusinessTableName (business_id, name, " +
    s"stars, review_counts, is_open, address, city, state, postal_code) VALUES (?,?,?,?,?,?,?,?,?)")
    val bestBusinessBinder = (b: Business, statement: PreparedStatement) => statement.bind(b.business_id, b.name, b.stars, b.review_count, b.is_open, b.address, b.city, b.state, b.postal_code)
    val bestBusinessFlow = CassandraFlow.createWithPassThrough[Business](parallelism = 2, bestBusinessStatement, bestBusinessBinder)

    val businessStatement = cSession.prepare(s"INSERT INTO $keyspaceName.$businessTableName (business_id, name, " +
      s"address, city, state, postal_code, latitude, longitude, " +
      "stars, review_counts, is_open, attributesRestaurantTakeOut, attributesWheelchairAccessible, attributesCaters, " +
      "attributesSmoking, attributesRestaurantReservations, attributesGoodForDancing, attributesOutdoorSeating, " +
      "attributesAlcohol, attributesHasTV, attributesCoatCheck, attributesDogsAllowed, attributesWifi, " +
      "attributesHappyHour, attributesBikeParking, attributesRestaurantTableService, attributesGoodForMeal, " +
      "attributesRestaurantsGoodForGroups, attributesRestaurantPriceRange2, attributesBusinessParking, attributesByAppointmentOnly, " +
      "attributesRestaurantAttire, attributesAmbiance, attributesGoodForKids, attributesNoiseLevel, " +
      "attributesRestaurantDelivery, attributesBusinessAcceptsCreditCards, categories, hoursMonday, " +
      "hoursTuesday, hoursWednesday, hoursThursday, hoursFriday, hoursSaturday, hoursSunday) " +
      "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
    val businessBinder = (b: Business, statement: PreparedStatement) => {
      val hours = b.hours.orNull
      val attributes = b.attributes.orNull
      // Parse nested dates
      var monday, tuesday, wednesday, thursday, friday, saturday, sunday: String = ""
      if(hours != null){
        monday = hours.Monday.getOrElse("")
        tuesday = hours.Tuesday.getOrElse("")
        wednesday = hours.Wednesday.getOrElse("")
        thursday = hours.Thursday.getOrElse("")
        friday = hours.Friday.getOrElse("")
        saturday = hours.Saturday.getOrElse("")
        sunday = hours.Sunday.getOrElse("")
      }
      // Parse nested attributes
      var restaurantTakeOut, wheelchairAccessible, smoking, caters, restaurantsReservations,
      goodForDancing, outdoorSeating, alcohol, hasTV, coatCheck, dogsAllowed, wifi, happyHour, bikeParking,
      restaurantsTableService, goodForMeal, restaurantsGoodForGroups, restaurantsPriceRange2, businessParking,
      byAppointmentOnly, restaurantsAttire, ambiance, goodForKids, noiseLevel, restaurantsDelivery,
      businessAcceptsCreditCards: String = ""
      if(attributes != null){
        restaurantTakeOut = attributes.RestaurantTakeOut.getOrElse("")
        wheelchairAccessible = attributes.WheelchairAccessible.getOrElse("")
        smoking = attributes.Smoking.getOrElse("")
        caters = attributes.Caters.getOrElse("")
        restaurantsReservations = attributes.RestaurantsReservations.getOrElse("")
        goodForDancing = attributes.GoodForDancing.getOrElse("")
        outdoorSeating = attributes.OutdoorSeating.getOrElse("")
        alcohol = attributes.Alcohol.getOrElse("")
        hasTV = attributes.HasTV.getOrElse("")
        coatCheck = attributes.CoatCheck.getOrElse("")
        dogsAllowed = attributes.DogsAllowed.getOrElse("")
        wifi = attributes.Wifi.getOrElse("")
        happyHour = attributes.HappyHour.getOrElse("")
        bikeParking = attributes.BikeParking.getOrElse("")
        restaurantsTableService = attributes.RestaurantsTableService.getOrElse("")
        goodForMeal = attributes.GoodForMeal.getOrElse("")
        restaurantsGoodForGroups = attributes.RestaurantsGoodForGroups.getOrElse("")
        restaurantsPriceRange2 = attributes.RestaurantsPriceRange2.getOrElse("")
        businessParking = attributes.BusinessParking.getOrElse("")
        byAppointmentOnly = attributes.ByAppointmentOnly.getOrElse("")
        restaurantsAttire = attributes.RestaurantsAttire.getOrElse("")
        ambiance = attributes.Ambiance.getOrElse("")
        goodForKids = attributes.GoodForKids.getOrElse("")
        noiseLevel = attributes.NoiseLevel.getOrElse("")
        restaurantsDelivery = attributes.RestaurantsDelivery.getOrElse("")
        businessAcceptsCreditCards = attributes.BusinessAcceptsCreditCards.getOrElse("")
      }

      statement
      .bind(b.business_id, b.name, b.address, b.city, b.state, b.postal_code, b.latitude, b.longitude, b.stars, b.review_count,
        b.is_open, restaurantTakeOut, wheelchairAccessible, smoking, caters, restaurantsReservations,
        goodForDancing, outdoorSeating, alcohol, hasTV, coatCheck, dogsAllowed, wifi, happyHour, bikeParking,
        restaurantsTableService, goodForMeal, restaurantsGoodForGroups, restaurantsPriceRange2, businessParking,
        byAppointmentOnly, restaurantsAttire, ambiance, goodForKids, noiseLevel, restaurantsDelivery,
        businessAcceptsCreditCards, b.categories.orNull,
        monday, tuesday, wednesday, thursday,
        friday, saturday, sunday)}
    val businessFlow = CassandraFlow.createWithPassThrough[Business](parallelism = 2, businessStatement, businessBinder)
    val businessResult = linesBusiness
      .via(businessFlow)
      .via(bestBusinessFlow)
      .runWith(Sink.ignore)

    val photoStatement = cSession.prepare(s"INSERT INTO $keyspaceName.$photoTableName (photo_id, caption, label, business_id) VALUES (?,?,?,?)")
    val photoBinder = (p: Photo, statement: PreparedStatement) => statement.bind(p.photo_id, p.caption, p.label, p.business_id)
    val photoFlow = CassandraFlow.createWithPassThrough[Photo](parallelism = 2, photoStatement, photoBinder)
    val photoResult = linesPhoto
      .via(photoFlow)
      .runWith(Sink.ignore)

    val checkinStatement = cSession.prepare(s"INSERT INTO $keyspaceName.$checkinTableName (business_id, date) VALUES (?,?)")
    val checkinBinder = (c: Checkin, statement: PreparedStatement) => statement.bind(c.business_id, c.date)
    val checkinFlow = CassandraFlow.createWithPassThrough[Checkin](parallelism = 2, checkinStatement, checkinBinder)
    val checkinResult = linesCheckin
        .via(checkinFlow)
        .runWith(Sink.ignore)

    val tipStatement = cSession.prepare(s"INSERT INTO $keyspaceName.$tipTableName (user_id, business_id, text, date, compliment_count) VALUES (?,?,?,?,?)")
    val tipBinder = (t: Tip, statement: PreparedStatement) => statement.bind(t.user_id, t.business_id, t.text, t.date, t.compliment_count)
    val tipFlow = CassandraFlow.createWithPassThrough[Tip](parallelism = 2, tipStatement, tipBinder)
    val tipResult = linesTip
      .via(tipFlow)
      .runWith(Sink.ignore)

    val reviewStatement = cSession.prepare(s"INSERT INTO $keyspaceName.$reviewTableName (review_id, user_id, business_id, stars, useful, funny, cool, text, date) VALUES (?,?,?,?,?,?,?,?,?)")
    val reviewBinder = (r: Review, statement: PreparedStatement) => statement.bind(r.review_id, r.user_id, r.business_id, r.stars, r.useful, r.funny, r.cool, r.text, r.date)
    val reviewFlow = CassandraFlow.createWithPassThrough[Review](parallelism = 2, reviewStatement, reviewBinder)
    val reviewResult = linesReview
      .via(reviewFlow)
      .runWith(Sink.ignore)

    val userStatement = cSession.prepare(s"INSERT INTO $keyspaceName.$userTableName " +
      s"(user_id, name, review_count, yelping_since, useful, funny, cool, elite, friends, fans, average_stars, compliment_hot, " +
      s"compliment_more, compliment_profile, compliment_cute, compliment_list, compliment_note, compliment_plain, compliment_cool, compliment_funny, compliment_writer, compliment_photos) " +
      s"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
    val userBinder = (u: User, statement: PreparedStatement) =>
      statement.bind(u.user_id, u.name, u.review_count, u.yelping_since, u.useful, u.funny, u.cool, u.elite, u.friends,
        u.fans, u.average_stars, u.compliment_hot, u.compliment_more, u.compliment_profile, u.compliment_cute, u.compliment_list,
        u.compliment_note, u.compliment_plain, u.compliment_cool, u.compliment_funny, u.compliment_writer, u.compliment_photos)
    val userFlow = CassandraFlow.createWithPassThrough[User](parallelism = 2, userStatement, userBinder)
    val userResult = linesUser
      .via(userFlow)
      .runWith(Sink.ignore)

    // Checking the results of the completed imports
    photoResult.onComplete(_ => checkTable(cSession, photoTableName))
    userResult.onComplete(_ => checkTable(cSession, userTableName))
    reviewResult.onComplete(_ => checkTable(cSession, reviewTableName))
    tipResult.onComplete(_ => checkTable(cSession, tipTableName))
    checkinResult.onComplete(_ => checkTable(cSession, checkinTableName))
    businessResult.onComplete(_ => checkTable(cSession, businessTableName))
  }

  /**
    * checkTables selects the first 25 rows of a table
    * @param cSession = The connection to Cassandra
    * @param name = The name of the table
    */
  def checkTable(cSession: Session, name: String): Unit ={
    printf(s"\n$name DONE!!!\n")
    val res = cSession.execute(s"SELECT * FROM $keyspaceName.$name LIMIT 25;")
    res.iterator().forEachRemaining(println)
  }

  /**
    * setupCassandra sets up a key space and all tables to import the data
    * Before creating anything new, it ensures the keyspace and table do not exists
    * @param cSession = The connection to Cassandra.
    */
  def setupCassandraDB(cSession: Session): Unit = {
    val keyspaceExists = cSession
        .execute( s"SELECT keyspace_name FROM system_schema.keyspaces WHERE keyspace_name='$keyspaceName';")
        .iterator().hasNext
    val existingTables = cSession
      .execute(s"SELECT table_name FROM system_schema.tables WHERE keyspace_name='$keyspaceName';").iterator()
    existingTables.forEachRemaining(table => mapOfTables(table.getString(0)) = true)

    if (!keyspaceExists){
      tryExecution(cSession, s"CREATE KEYSPACE $keyspaceName WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1};")
    }
    if(tableDoesNotExists(bestBusinessTableName)) {
      tryExecution(cSession, s"CREATE TABLE $keyspaceName.$bestBusinessTableName " +
        "(business_id text, name text, stars float, review_counts int, is_open int, address text, city text, state text, postal_code text, " +
        "PRIMARY KEY((name, business_id), stars)) " +
        "WITH comment = 'Best reviewed businesses' AND CLUSTERING ORDER BY (stars DESC);")
    }
    if(tableDoesNotExists(businessTableName)){
      tryExecution(cSession, s"CREATE TABLE $keyspaceName.$businessTableName " +
        "(business_id text, name text, address text, city text, state text, postal_code text, latitude double, longitude double, " +
        "stars float, review_counts int, is_open int, attributesRestaurantTakeOut text, attributesWheelchairAccessible text, attributesCaters text," +
        "attributesSmoking text, attributesRestaurantReservations text, attributesGoodForDancing text, attributesOutdoorSeating text, " +
        "attributesAlcohol text, attributesHasTV text, attributesCoatCheck text, attributesDogsAllowed text, attributesWifi text, " +
        "attributesHappyHour text, attributesBikeParking text, attributesRestaurantTableService text, attributesGoodForMeal text, " +
        "attributesRestaurantsGoodForGroups text, attributesRestaurantPriceRange2 text, attributesBusinessParking text, attributesByAppointmentOnly text, " +
        "attributesRestaurantAttire text, attributesAmbiance text, attributesGoodForKids text, attributesNoiseLevel text, " +
        "attributesRestaurantDelivery text, attributesBusinessAcceptsCreditCards text, categories text, hoursMonday text, " +
        "hoursTuesday text, hoursWednesday text, hoursThursday text, hoursFriday text, hoursSaturday text, hoursSunday text, " +
        "PRIMARY KEY(business_id)) " +
        "WITH comment = 'All businesses';")
    }
    if(tableDoesNotExists(photoTableName)) {
    tryExecution(cSession, s"CREATE TABLE $keyspaceName.$photoTableName " +
      "(photo_id text, caption text, label text, business_id text, " +
      "PRIMARY KEY (photo_id, business_id)) " +
      "WITH comment='Photos from businesses';")
    }
    if(tableDoesNotExists(checkinTableName)) {
    tryExecution(cSession, s"CREATE TABLE $keyspaceName.$checkinTableName " +
      "(business_id text, date text, " +
      "PRIMARY KEY (business_id)) " +
      "WITH comment='Business seen at dates';")
    }
    if(tableDoesNotExists(userTableName)) {
    tryExecution(cSession, s"CREATE TABLE $keyspaceName.$userTableName " +
      "(user_id text, name text, review_count int,  yelping_since text, useful int, funny int, cool int, elite text, " +
      "friends text, fans int, average_stars float, compliment_hot int, compliment_more int, compliment_profile int, " +
      "compliment_cute int, compliment_list int, compliment_note int, compliment_plain int, compliment_cool int, " +
      "compliment_funny int, compliment_writer int, compliment_photos int, " +
      "PRIMARY KEY (user_id)) " +
      "WITH comment='User data';")
    }
    if(tableDoesNotExists(tipTableName)) {
    tryExecution(cSession, s"CREATE TABLE $keyspaceName.$tipTableName " +
      "(user_id text, business_id text, text text, date text, compliment_count int, " +
      "PRIMARY KEY (user_id, business_id)) " +
      "WITH comment='Tips from users about businesses';")
    }
    if(tableDoesNotExists(reviewTableName)) {
      tryExecution(cSession, s"CREATE TABLE $keyspaceName.$reviewTableName " +
        "(review_id text, user_id text, business_id text, stars float, useful int, funny int, cool int, text text, date text," +
        "PRIMARY KEY ((review_id), business_id, user_id)) " +
        "WITH comment='Reviews from users about businesses';")
    }
  }

  /**
    * Wrapper for critical queries
    * @param cSession = Cassadra session
    * @param query = query to run on Cassandra as a string
    * @return the query result
    */
  def tryExecution(cSession: Session, query: String): Option[ResultSet] ={
    try {
      Some(cSession.execute(query))
    } catch {
      case _: Throwable => {
        println("ERROR! Could not execute: " + query)
        None
      }
    }
  }

  /**
    * Check if table is already in map
    * @param name is the name of the table
    * @return a boolean
    */
  def tableDoesNotExists(name: String): Boolean ={
    !mapOfTables(name)
  }

  /**
    *  importDataFromFile takes a String to a file and returns an Akka Streams Source which reads the contents
    *  line by line and parses the bytes to string as utf-8
    * @param file: string to file
    * @return Akka Streams Source[String, Future[IOResult] ]
    */
  def importDataFromFile(file: String): Source[String, Future[IOResult]]={
    FileIO.fromPath(Paths.get(file))
      .via(Framing.delimiter(ByteString("\n"), maximumFrameLength = 10000000, allowTruncation = false))
      .map(_.decodeString("utf-8"))
  }
}
